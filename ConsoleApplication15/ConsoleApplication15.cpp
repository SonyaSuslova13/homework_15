﻿#include <iostream>
#include <cmath>

void value(int N, int a)

{
	for (int i = a; i <= N; i+= 2)
		std::cout << i << " ";
	std::cout << "\n";
}

int main()
{
	int N;
	std::cout << "Enter value N = "; std::cin >> N;

	std::cout << "Even numbers:\n";
	value(N, 0);

	std::cout << "Odd numbers:\n";
	value(N, 1);
	return 0;
}